# https://aur.archlinux.org/packages/bitwarden
pkgname=bitwarden
pkgver=1.24.6
pkgrel=1
pkgdesc='A secure and free password manager for all of your devices.'
arch=('x86_64')
url="https://bitwarden.com"
license=('GPL3')
depends=('electron9' 'libnotify' 'libsecret' 'libxtst')
makedepends=('git' 'npm' 'python' 'nodejs-lts-erbium' 'jq')
optdepends=('libappindicator-gtk3: for tray icon')
source=("git+https://github.com/bitwarden/desktop.git#tag=v$pkgver"
        'git+https://github.com/bitwarden/jslib.git'
        'package.json.patch'
        "$pkgname.sh"
        "$pkgname.desktop")
b2sums=('SKIP'
        'SKIP'
        '9d6cf17d47915bb6bb9dc1920e15f8640e4873711b774873913d198339f36e1a6172fc1710a8ccfaab518ff48e07525bf0451110ac28dc095566052eab8bd89f'
        '080c8485f2e93374b8ab0892a4494eb411dd3d934cc42c1d02cbbd4c85634d89d5bec405f13a3c44859947aaeae7297f769c2a3b6d4d1810477f9ae23ee37ca5'
        'e3a3658e2fe5dbcd88d5d7fd977c4f12f6c67d911e30a1cb09ef62e97ed7df1437ad6d9b2babedf53cc050bd01a6b49b6b411a4817c84e789bd170d8b031365c')

prepare() {
	cd "$srcdir/desktop"
	# Link jslib
	git submodule init
	git config submodule.jslib.url "$srcdir/jslib"
	git submodule update

	# Patch out postinstall routines
	patch --strip=1 package.json "$srcdir/package.json.patch"

	# Patch build to make it work with system electron
	SYSTEM_ELECTRON_VERSION=$(tail /usr/lib/electron9/version)
	jq < package.json --arg ver $SYSTEM_ELECTRON_VERSION\
	'.build["electronVersion"]=$ver | .build["electronDist"]="/usr/lib/electron9"'\
	> package.json.patched
	mv package.json.patched package.json

	export npm_config_cache="$srcdir/npm_cache"
}

build() {
	export ELECTRON_SKIP_BINARY_DOWNLOAD=1

	cd "$srcdir/desktop/jslib"
	HOME="$srcdir/.electron-gyp" npm install

	cd "$srcdir/desktop"
	HOME="$srcdir/.electron-gyp" npm install
	npm run build
	npm run clean:dist
	npx electron-builder build --dir
}

package() {
	install -D     $pkgname.sh "$pkgdir/usr/bin/bitwarden-desktop"
	install -Dm644 $pkgname.desktop -t "$pkgdir/usr/share/applications"

	cd "$srcdir/desktop"
	install -d "$pkgdir/usr/lib/$pkgname"
	cp -r dist/linux-unpacked/resources "$pkgdir/usr/lib/$pkgname"

	for i in 16 32 48 64 128 256 512; do
		install -Dm644 resources/icons/${i}x${i}.png "$pkgdir/usr/share/icons/hicolor/${i}x${i}/apps/$pkgname.png"
	done
	install -Dm644 resources/icon.png "$pkgdir/usr/share/icons/hicolor/1024x1024/apps/$pkgname.png"
}
